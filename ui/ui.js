define('two/chef/ui', ['two/chef', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(KingTool, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var bindEvents = function() {
    }
    var ChefInterface = function() {
        ui = new Interface('KingTool', {
            activeTab: 'chef',
            template: '__chef_html_window',
            css: '__chef_css_style',
            replaces: {
                locale: Locale,
                version: KingTool.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('chef', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        bindEvents()
        KingTool.interfaceInitialized = true
        return ui
    }
    KingTool.interface = function() {
        KingTool.interface = ChefInterface()
    }
})