define('two/chef', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var KingTool = {}
    KingTool.version = '__chef_version'
    KingTool.init = function () {
        Locale.create('chef', __chef_locale, 'pl')

        KingTool.initialized = true
    }
    KingTool.run = function () {
        if (!KingTool.interfaceInitialized) {
            throw new Error('KingTool interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return KingTool
})