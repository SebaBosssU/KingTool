require([
    'two/ready',
    'two/chef',
    'two/chef/ui'
], function (
    ready,
    KingTool
) {
    if (KingTool.initialized) {
        return false
    }

    ready(function () {
        KingTool.init()
        KingTool.interface()
        KingTool.run()
    })
})
